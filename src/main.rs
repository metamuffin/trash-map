pub mod dimension;
pub mod render;
pub mod tiling;
pub mod viewer;

use clap::{Parser, Subcommand};
use dimension::Dimension;
use std::{net::SocketAddr, path::PathBuf};
use viewer::serve_http;

#[derive(Parser)]
struct Args {
    /// Dimension directory containing .mca files
    #[clap(long, short, default_value = "./world/region")]
    dimension: PathBuf,

    #[clap(subcommand)]
    action: Action,
}
#[derive(Subcommand)]
enum Action {
    /// Run a http server providing an interactive map viewer.
    Serve {
        #[arg(long, short, default_value = "127.0.0.1:8080")]
        bind: SocketAddr,
    },
}

fn main() {
    env_logger::builder()
        .filter_level(log::LevelFilter::Info)
        .parse_env("LOG")
        .init();

    let args = Args::parse();

    let dimension = Dimension::new(args.dimension);

    match args.action {
        Action::Serve { bind } => serve_http(dimension, bind),
    }
}
