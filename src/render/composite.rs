use image::ImageBuffer;
use image::Rgba;
pub const REGION_SIZE: usize = 16 * 8;
pub const CHUNK_HEIGHT: usize = 320;
pub const CHUNK_SIZE: usize = 16;

pub fn image_buffer_blit(
    target: &mut ImageBuffer<Rgba<u8>, Vec<u8>>,
    source: &ImageBuffer<Rgba<u8>, Vec<u8>>,
    offset: (isize, isize),
) {
    for (x, y, source_pixel) in source.enumerate_pixels() {
        let tx = x as isize + offset.0;
        let ty = y as isize + offset.1;
        if tx >= 0 && tx < target.width() as isize && ty >= 0 && ty < target.height() as isize {
            let target_pixel = target.get_pixel_mut(tx as u32, ty as u32);
            let sa = source_pixel.0[3] as u16;
            let new_pixel = Rgba::from([
                ((target_pixel.0[0] as u16 * (255 - sa)) / 255
                    + (source_pixel.0[0] as u16 * sa) / 255) as u8,
                ((target_pixel.0[1] as u16 * (255 - sa)) / 255
                    + (source_pixel.0[1] as u16 * sa) / 255) as u8,
                ((target_pixel.0[2] as u16 * (255 - sa)) / 255
                    + (source_pixel.0[2] as u16 * sa) / 255) as u8,
                255 - (((255 - target_pixel.0[3] as u16) * (255 - sa)) / 255) as u8,
            ]);
            *target_pixel = new_pixel;
        }
    }
}

// pub fn isometric_coord_mapping(x: i32, y: i32, z: i32) -> (u32, u32) {
//     // const BASE_X: i32 = 1016;
//     const BASE_X: i32 = (SEG_SIZE as i32 * 8) - 8;
//     const BASE_Y: i32 = 2040;

//     const XDIFF: (i32, i32) = (-8, 4);
//     const ZDIFF: (i32, i32) = (8, 4);
//     const YDIFF: (i32, i32) = (0, -8);

//     let diff = (
//         XDIFF.0 * x + YDIFF.0 * y + ZDIFF.0 * z,
//         XDIFF.1 * x + YDIFF.1 * y + ZDIFF.1 * z,
//     );

//     let coords = (BASE_X + diff.0, BASE_Y + diff.1);

//     (coords.0 as u32, coords.1 as u32)
// }
