use std::{
    fs::File,
    io::Read,
    path::{Path, PathBuf},
};

use log::info;

use crate::render::Renderer;

pub struct Tiler {
    renderer: Renderer,
}

impl Tiler {
    pub fn new(renderer: Renderer) -> Self {
        std::fs::create_dir_all("tiles").unwrap();
        Self { renderer }
    }
    pub fn path(z: isize, x: isize, y: isize) -> PathBuf {
        Path::new(&format!("tiles/{},{},{}.png", z, x, y)).to_path_buf()
    }

    pub fn generate_tile(&self, z: isize, x: isize, y: isize) {
        info!("generating tile (zoom={z}, x={x}, y={z})");
        let segment = self.renderer.render_segment(x, y);
        segment.save(Tiler::path(z, x, y)).unwrap();
    }

    pub fn update_tile(&self, z: isize, x: isize, y: isize) {
        // TODO check mtime of region
        if Tiler::path(z, x, y).exists() {
            return; // already generated
        }
        self.generate_tile(z, x, y)
    }

    pub fn get_tile(&self, z: isize, x: isize, y: isize) -> Vec<u8> {
        self.update_tile(z, x, y);
        let mut f = File::open(Tiler::path(z, x, y)).unwrap();
        let mut buf = Vec::new();
        f.read_to_end(&mut buf).unwrap();
        buf
    }
}
