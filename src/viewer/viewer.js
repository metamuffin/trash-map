
const map = L.map('map', { 
    crs: L.CRS.Simple
})

map.setView([0, 0], 13);

L.tileLayer('/tiles/{z}/{x}/{y}', {
    maxZoom: 13,
    minZoom: 13,
    attribution: 'trash-map'
}).addTo(map);
